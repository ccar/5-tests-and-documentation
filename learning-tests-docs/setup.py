# -*- coding: utf-8 -*-
"""Setup file for mytestdoc package

To install, run following command in Anaconda prompt/terminal:
    pip install -e .
"""


from setuptools import setup

setup(name='mytestdocs',  # name of package on import
      version='0.1',  # package version
      description='Demo for teaching tests and docs',  # brief description
      url='https://gitlab.windenergy.dtu.dk/python-at-risoe/' +
      'scientific-python-workshops/5-tests-and-documentation',  # git repo url
      author='Jenni Rinker',  # author(s)
      author_email='rink@dtu.dk',  # email
      license='GNU GPL',  # licensing
      packages=['mytestdocs'],  # names of folders
      zip_safe=True)  # package can be installed from zip file
